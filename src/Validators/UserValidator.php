<?php

namespace Sunnydevbox\TWUser\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class UserValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'email'         => 'required|email',
            'password'      => 'min:8',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'email'         => 'email',
            'password'      => 'min:8',
        ]
   ];

}