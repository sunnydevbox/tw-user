<?php

namespace Sunnydevbox\TWUser\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Facades\Artisan;

class PublishMigrationsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twuser:publish-migrations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish TWUser migrations files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $exitCode = Artisan::call('vendor:publish', [
            '--provider'    => 'Sunnydevbox\TWUser\TWUserServiceProvider',
            '--tag'        => 'migrations',
        ]);

        $this->info('Copying PERMISSIONS config');
        $exitCode = Artisan::call('vendor:publish', [
            '--provider'    => 'Spatie\Permission\PermissionServiceProvider',
            '--tag'        => 'config',
        ]);
        $this->info('...DONE');

        $this->info('Copying PERMISSIONS migrations');
        $exitCode = Artisan::call('vendor:publish', [
            '--provider'    => 'Spatie\Permission\PermissionServiceProvider',
            '--tag'        => 'migrations',
        ]);
        $this->info('...DONE');
    }

    public function fire()
    {
        echo 'fire';
    }
}
