<?php

namespace Sunnydevbox\TWUser\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Facades\Artisan;

class PublishConfigCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twuser:publish-config';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish TWUser config files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Publishing TWUser config');
        $exitCode = Artisan::call('vendor:publish', [
            '--provider'    => 'Sunnydevbox\TWUser\TWUserServiceProvider',
            '--tag'        => 'config',
        ]);
        $this->info('...DONE');

        $this->info('Copying TWUser assets');
        $exitCode = Artisan::call('vendor:publish', [
            '--provider'    => 'Spatie\Permission\PermissionServiceProvider',
            '--tag'        => 'assets',
        ]);
        $this->info('...DONE');


        $this->info('Copying PERMISSIONS config');
        $exitCode = Artisan::call('vendor:publish', [
            '--provider'    => 'Spatie\Permission\PermissionServiceProvider',
            '--tag'        => 'config',
        ]);
        $this->info('...DONE');
    }

    public function fire()
    {
        echo 'fire';
    }
}
