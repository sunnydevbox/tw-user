<?php

namespace Sunnydevbox\TWUser\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Facades\Artisan;

class SetupCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twuser:run-setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish TWUser migrations files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // RUN publish config / migrations
        $exitCode = Artisan::call('twuser:publish-config');
        $exitCode = Artisan::call('twuser:publish-migrations');
        // RUN migration
    }

    public function fire()
    {
        echo 'fire';
    }
}
