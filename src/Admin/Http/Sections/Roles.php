<?php

namespace Sunnydevbox\TWUser\Admin\Http\Sections;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminColumnFilter;
use AdminDisplayFilter;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Badge;
use SleepingOwl\Admin\Section;

use Request;

/**
 * Class Pages
 *
 * @property \App\Model\Page $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Roles extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = null;

    /**
     * @var string
     */
    protected $alias = null;


    protected $model = '\Sunnydevbox\TWUser\Models\Role';

    /**
     * Initialize class.
     */
    public function initialize()
    {

        $config = config('tw-user.admin.roles');

        if ($config['alias']) {
            $this->alias = $config['alias'];
        }

        if ($config['title']) {
            $this->title = $config['title'];
        }

        if ($config['model']) {
            $this->model = new $config['model'];
        }

        /*$this->addToNavigation(9999999, 10)
            ->setIcon('fa ' . $config['icon']);
            ;*/
    }

    public function onDisplay()
    {
        $display = AdminDisplay::table()->setHtmlAttribute('class', 'table-primary')->setApply(function($query) {
            
        })->paginate(30);


        $display->setColumns([
            $name = AdminColumn::text('proper_name', 'Name'),
        ]);

        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::form()->setElements([
            AdminFormElement::text('name', 'Name')->required(),
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }
}
