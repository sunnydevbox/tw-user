<?php

namespace Sunnydevbox\TWUser\Admin\Http\Sections;

use AdminColumn;
use AdminColumnEditable;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminColumnFilter;
use AdminDisplayFilter;
use SleepingOwl\Admin\Form\FormElements;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Navigation\Badge;
use SleepingOwl\Admin\Section;

use Sunnydevbox\TWUser\Repositories\Role\RoleRepository;
use Sunnydevbox\TWUser\Repositories\User\UserRepository;

use Illuminate\Http\Request;

/**
 * Class Pages
 *
 * @property \App\Model\Page $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Users extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = null;

    /**
     * @var string
     */
    protected $alias = null;


    protected $model = '\Sunnydevbox\TWUser\Models\User';

    /**
     * Initialize class.
     */
    public function initialize()
    {

        $config = config('tw-user.admin.users');

        if ($config['alias']) {
            $this->alias = $config['alias'];
        }

        if ($config['title']) {
            $this->title = $config['title'];
        }

        if ($config['model']) {
            $this->model = new $config['model'];
        }

        /*$this->addToNavigation(9999999, 10)
            ->setIcon('fa ' . $config['icon']);
            ;*/
    }

    public function onDisplay()
    {
        $this->registerEvent('modify', function(){
            dd(22);
        }, 0);
        //dd($this->getEventDispatcher());
        

        //dd($this->display());
        //dd(Request::all());
        $display = AdminDisplay::datatablesAsync()->setHtmlAttribute('class', 'table-primary')->setApply(function($query) {
            //var_dump($query->getBindings());
            //$query->whereNull('deleted_at');
        })
            //->setDisplaySearch(true)
            ->setActions([
                AdminColumn::action('export', 'Export')->setAction(route('admin.news.export'))->setIcon('fa fa-share')
            ])
            ->paginate(5);

        /*$display->setFilters([
            AdminDisplayFilter::field('first_name')
                ->setAlias('category')
                ->setTitle('Category ID [:value]'),
        ]);*/


       /* $display->setColumnFilters([
            null,
            AdminColumnFilter::text()->setPlaceholder('Full Name'),
            AdminColumnFilter::range()->setFrom(
                AdminColumnFilter::date()->setPlaceholder('From Date')->setFormat('M d, Y')
            )->setTo(
                AdminColumnFilter::date()->setPlaceholder('To Date')->setFormat('M d, Y')
            ),
        ]);*/

        $display->setColumns([
            $full_name = AdminColumn::custom('Name', function(\Illuminate\Database\Eloquent\Model $model) {
                return $model->full_name;
            })->setOrderable(function($query, $direction) {
                $query->orderBy('last_name', $direction);
            }),
            //AdminColumn::text('first_name', 'Name')->setView('columns.full_name'),
            AdminColumn::text('email', 'Email'),
            AdminColumn::datetime('created_at', 'Date Joined')->setFormat('M d, Y'),
        ]);

        $display->getActions()->setPlacement('panel.buttons')->setHtmlAttribute('class', 'pull-right');
        $display->getActions()->setPlacement('panel.buttons')->setHtmlAttribute('class', 'pull-right');
    
        
        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit(
        UserRepository $rpoUser,
        RoleRepository $rpoRole, 
        Request $request, 
        $id
    ) {

        if (!empty($request->all())) {
            if (strtolower($request->get('type')) == 'roles') {
                $rpoUser->syncRoles($request->get('id'), $request->get('roles'));
                /*dd($request->get('roles'));
                dd($rpoUser->find($request->get('id')));
                dd($request->all());*/
            }
            
        }
        


        $tabs = AdminDisplay::tabbed();

        $panel_primary = AdminForm::panel();

        $panel_primary->setItems(
            AdminFormElement::columns()
            ->addColumn(function() {
                return [
                    AdminFormElement::text('first_name', 'First Name')->required(),
                    AdminFormElement::text('last_name', 'Last Name')->required(),
                    AdminFormElement::text('email', 'Email')->required()->unique(),
                    AdminFormElement::password('password', 'Password')->allowEmptyValue()->hashWithBcrypt(),
                ];
            })->addColumn(function() {
                return [
                    /*AdminFormElement::image('photo', 'Photo'),
                    AdminFormElement::date('birthday', 'Birthday')->setFormat('d.m.Y'),*/
                ];
            })
        );


        //$Role = config('tw-user.admin.roles.model');
        //dd(new {(config('tw-user.admin.roles.model'))});
        
        $roles_tab_fields = [];

        foreach($rpoRole->orderBy('name', 'asc')->all() as $role) {
            //dd($role->proper_name);
            $roles_tab_fields[] = AdminFormElement::checkbox('roles[' . $role->name. ']', $role->proper_name);
        }
        
        $roles_tab_fields[] = AdminFormElement::custom()->setDisplay(function($model) {
            return '<input type="hidden" name="type" value="roles" /><input type="hidden" name="id" value="'.$model->id.'" />';
        });

        $form_roles = AdminForm::form()->addElement(
            new FormElements($roles_tab_fields)
            
        ); 


        $tabs->appendTab($panel_primary,  'Personal Information');
     
        $tabs->appendTab($form_roles,     'Roles');

        $tabs->appendTab($form_roles,     'Reset Email/Password');

        //$tabs->appendTab($formVisual,   'Visual Adress Redactor');


        return $tabs;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

}
