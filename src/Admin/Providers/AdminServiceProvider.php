<?php

namespace Sunnydevbox\TWUser\Admin\Providers;

use Admin\Http\Sections\Contacts;
use Admin\Policies\ContactsSectionModelPolicy;
use Illuminate\Routing\Router;
use SleepingOwl\Admin\Contracts\Navigation\NavigationInterface;
use SleepingOwl\Admin\Contracts\Template\MetaInterface;
use SleepingOwl\Admin\Contracts\Widgets\WidgetsRegistryInterface;
use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;
use SleepingOwl\Admin\Admin;

class AdminServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    protected $widgets = [
        //\Admin\Widgets\DashboardMap::class,
        //\Admin\Widgets\NavigationUserBlock::class
    ];

    /**
     * @var array
     */

    protected $sections = [];

    /**
     * @param \SleepingOwl\Admin\Admin $admin
     *
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin)
    {
        $this->loadViewsFrom(base_path("admin/resources/views"), 'admin');
        $this->registerPolicies('Admin\\Policies\\');

        $this->app->call([$this,'registerSleepingOwlSections']);

        $this->app->call([$this, 'registerRoutes']);
        $this->app->call([$this, 'registerNavigation']);

        parent::boot($admin);

        $this->app->call([$this, 'registerViews']);
        $this->app->call([$this, 'registerMediaPackages']);

        
    }

    public function register()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/', 'sleeping_owl');
    }




    public function registerSleepingOwlSections(\SleepingOwl\Admin\Admin $admin)
    {
        


        if (config('tw-user.admin.users.integrate')) {

            $admin->registerSections([
                config('tw-user.admin.users.model') => config('tw-user.admin.users.section'),
                config('tw-user.admin.roles.model') => config('tw-user.admin.roles.section'),
                config('tw-user.admin.permissions.model') => config('tw-user.admin.permissions.section'),
            ]);
            
        }
        
    }


    /**
     * @param NavigationInterface $navigation
     */
    public function registerNavigation(NavigationInterface $navigation)
    { 
        require __DIR__ . '/../navigation.php';
    }

    /**
     * @param WidgetsRegistryInterface $widgetsRegistry
     */
    public function registerViews(WidgetsRegistryInterface $widgetsRegistry)
    {
        foreach ($this->widgets as $widget) {
            $widgetsRegistry->registerWidget($widget);
        }
    }

    /**
     * @param Router $router
     */
    public function registerRoutes(Router $router)
    {
        $router->group([
                'prefix' => config('sleeping_owl.url_prefix'), 
                'middleware' => config('sleeping_owl.middleware')
            ], 
            function ($router) {
                require __DIR__ . '/../routes.php';
            }
        );
    }

    /**
     * @param MetaInterface $meta
     */
    public function registerMediaPackages(MetaInterface $meta)
    {
        $packages = $meta->assets()->packageManager();

        //require base_path('admin/assets.php');
    }
}
