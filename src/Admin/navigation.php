<?php

/**
 * @var \SleepingOwl\Admin\Contracts\Navigation\NavigationInterface $navigation
 * @see http://sleepingowladmin.ru/docs/menu_configuration
 */

use SleepingOwl\Admin\Navigation\Page;

$user_pages = [];



if (config('tw-user.admin.users.integrate')) {

    $user_pages[] = (new Page(config('tw-user.admin.users.model')))
                ->setIcon('fa ' . config('tw-user.admin.users.icon'))
                ->setPriority(0)
                ->setTitle(config('tw-user.admin.users.title'));
}


if (config('tw-user.admin.roles.integrate')) {

    $user_pages[] = (new Page(config('tw-user.admin.roles.model')))
                ->setIcon('fa ' . config('tw-user.admin.roles.icon'))
                ->setPriority(0)
                ->setTitle(config('tw-user.admin.roles.title'));
}


if (config('tw-user.admin.permissions.integrate')) {

    $user_pages[] = (new Page(config('tw-user.admin.permissions.model')))
                ->setIcon('fa ' . config('tw-user.admin.permissions.icon'))
                ->setPriority(0)
                ->setTitle(config('tw-user.admin.permissions.title'));
}

//dd(class_exists(config('tw-user.admin.users.section')));

//dd($user_pages);

$navigation->setFromArray([
    [
        'title' => 'User Management',
        'icon' => 'fa fa-users',
        'priority' => '1000',
        'badge' => new \SleepingOwl\Admin\Navigation\Badge(1),
        'pages' => $user_pages,

        /*[
            (new Page(\Sunnydevbox\TWUser\Models\User::class))
                ->setIcon('fa ' . config('tw-user.admin.user.icon'))
                ->setPriority(0),

            (new Page(\Sunnydevbox\TWUser\Models\Role::class))
                ->setIcon('fa fa-fax')
                ->setPriority(1),
            (new Page(\Sunnydevbox\TWUser\Models\Permission::class))
                ->setIcon('fa fa-fax')
                ->setPriority(2),*/
            /*(new Page(\App\Model\Contact2::class))
                ->setIcon('fa fa-fax')
                ->setPriority(100),
            (new Page(\App\Model\Contact3::class))
                ->setIcon('fa fa-fax')
                ->setPriority(200),
            (new Page(\App\Model\Contact4::class))
                ->setIcon('fa fa-fax')
                ->setPriority(400),*/
        //]
    ],
]);
