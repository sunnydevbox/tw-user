<?php

namespace Sunnydevbox\TWUser;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [

        // User signed up for an account
        'Sunnydevbox\TWUser\Events\UserRegisteredEvent' => [
            'Sunnydevbox\TWUser\Listeners\SendActivationCodeEventListener',
        ],
        
        // User requested to reset his password
        'Sunnydevbox\TWUser\Events\UserRequestResetPasswordEvent' => [
            'Sunnydevbox\TWUser\Listeners\SendResetPasswordTokenEventListener',
        ],

        // USER Verified his email address
        'Sunnydevbox\TWUser\Events\UserVerifiedEmailEvent' => [
            'Sunnydevbox\TWUser\Listeners\VerifiedAccount',
            'Sunnydevbox\TWUser\Listeners\SendEmailVerificationConfirmation',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
