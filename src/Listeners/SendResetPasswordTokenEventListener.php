<?php

namespace Sunnydevbox\TWUser\Listeners;

use Sunnydevbox\TWUser\Events\UserRequestResetPasswordEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Sunnydevbox\TWUser\Mail\ResetPassword;

class SendResetPasswordTokenEventListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(UserRequestResetPasswordEvent $event)
    {
        \Log::info('sending_reset_password_token', [
            'id' => $event->user->id ,
            'email' => $event->user->email,
        ]);

        // DO THE EMAIL
        Mail::to($event->user->email)->send(new ResetPassword($event->user));
    }
}
