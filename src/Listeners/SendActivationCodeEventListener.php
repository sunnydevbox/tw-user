<?php

namespace Sunnydevbox\TWUser\Listeners;

use Sunnydevbox\TWUser\Events\UserRegisteredEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Sunnydevbox\TWUser\Mail\UserRegistered;

class SendActivationCodeEventListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(UserRegisteredEvent $event)
    {
        \Log::info('sending_activation_code', [
            'id' => $event->user->id ,
            'email' => $event->user->email,
        ]);

        // DO THE EMAIL
        Mail::to($event->user->email)->send(new UserRegistered($event->user));
    }
}
