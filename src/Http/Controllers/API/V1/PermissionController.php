<?php

namespace Sunnydevbox\TWUser\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class PermissionController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWUser\Repositories\Permission\PermissionRepository $repository, 
		\Sunnydevbox\TWUser\Validators\PermissionValidator $validator,
		\Sunnydevbox\TWUser\Transformers\PermissionTransformer $transformer
	) {
        $this->repository = $repository;
        $this->validator  = $validator;
		$this->transformer = $transformer;
    }
}