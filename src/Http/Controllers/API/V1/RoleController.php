<?php

namespace Sunnydevbox\TWUser\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Dingo\Api\Http\Request;
use Illuminate\Api\Http\Response;

class RoleController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWUser\Repositories\Role\RoleRepository $repository, 
		\Sunnydevbox\TWUser\Validators\RoleValidator $validator,
		\Sunnydevbox\TWUser\Transformers\RoleTransformer $transformer
	) {
		$this->repository = $repository;
		$this->validator  = $validator;
		$this->transformer = $transformer;
	}

	public function attachPermission(Request $request, $roleId)
	{
		$role = $this->repository->find($roleId);

		$role->givePermissionTo($request->get('name'));
		return $this->response->noContent();
	}

	public function revokePermission(Request $request, $roleId)
	{
		$role = $this->repository->find($roleId);
		
		$role->revokePermissionTo($request->get('name'));

		return $this->response->noContent();
	}

	public function getPermissions($roleId)
	{

		$role = $this->repository->find($roleId);
		return response()->json([
			'data' => $role->permissions->map(function ($permission) {
				return collect($permission->toArray())
					->only(['id', 'name'])
					->all();
				})->toArray()
		], 200);
		
	}
}