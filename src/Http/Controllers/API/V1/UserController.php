<?php

namespace Sunnydevbox\TWUser\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\TWUser\Transformers\PermissionTransformer;
use Sunnydevbox\TWUser\Transformers\RoleTransformer;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Input;
use Prettus\Validator\Contracts\ValidatorInterface;
use Sunnydevbox\TWUser\Events\UserRegisteredEvent;
use Sunnydevbox\TWUser\Events\UserVerifiedEmailEvent;
use Prettus\Validator\Contracts\ValidatorException;

class UserController extends APIBaseController
{
	protected $return_messages = [
		'object_not_found'	=> 'User does not exist',
		'400' 				=> 'Invalid request',
		'success' 			=> 'Operation successful.'
	];

	public function __construct(
		\Sunnydevbox\TWUser\Repositories\User\UserRepository $repository, 
		\Sunnydevbox\TWUser\Validators\UserValidator $validator,
		\Sunnydevbox\TWUser\Transformers\UserTransformer $transformer
	) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
	}

    public function show($id)
	{
		$result = $this->repository->find($id);

		if (!$result && isset($this->return_messages)) {
			return $this->response->error($this->return_messages['object_not_found'], 404);
        }
            
		if (Input::get('mode') && Input::get('mode') == 'complete') {
			$this->transformer->setMode('complete');
        }

		return $this->response->item($result, $this->transformer);
	}

	public function removeRole(Request $request, $id)
	{
		if (!empty($request->get('roles'))) {
			$user = $this->repository->find($id);
			if ($user) {
				$user->removeRole($request->get('roles'));
			}

			return $this->response->noContent();
		}

		return $this->response->errorBadRequest('Missing roles');
	}

	public function assignRoles(Request $request, $id)
	{
		if (!empty($request->get('roles'))) {
			$user = $this->repository->find($id);
			
			if ($user) {
				if (is_array($request->get('roles'))) {

					$user->syncRoles($request->get('roles'));
					
				} else if (is_string($request->get('roles'))) {

					$user->assignRole($request->get('roles'));

				}

				return response()->json([
					'status_code'   => 200,
					'message' => 'Roles assigned'
				], 200);
			}
		}

		return $this->response->errorBadRequest('Missing roles');
	}

	public function getRoles(RoleTransformer $roleTransformer, $id)
	{
		$user = $this->repository->find($id);
		
		if ($user) {
			return $this->response->collection($user->roles()->get(), $roleTransformer);
			
			// return response()->json([
            //     'status_code'   => 200,
            //     'roles' => $user->getRoleNames()
            // ], 200);
		}
	}

	public function assignPermissions(Request $request, $id)
	{
		if (!empty($request->get('permissions'))) {
			$user = $this->repository->find($id);
			
			if ($user) {
				if (is_array($request->get('permissions'))) {

					$user->syncPermissions($request->get('permissions'));

				} else if (is_string($request->get('permissions'))) {

					$user->givePermissionTo($request->get('permissions'));

				}

				return response()->json([
					'status_code'   => 200,
					'message' => 'Permissions granted'
				], 200);
			}
		}

		return $this->response->errorBadRequest('Missing permissions');
	}

	public function getPermissions(PermissionTransformer $permissionTransformer, $id)
	{
		$user = $this->repository->find($id);
		
		if ($user) {
			
			return $this->response->collection($user->getAllPermissions(), $permissionTransformer);

			// return response()->json([
            //     'status_code'   => 200,
            //     'roles' => $user->getAllPermissions()
            // ], 200);
		}
	}


	public function register(Request $request)
    {  
        try {
			
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
			$result = $this->repository->register($request);
			
			// NOTIFY USER
            event(new UserRegisteredEvent($result));
           
            return $this->response->item($result, $this->transformer);
        } catch (ValidatorException $e) {
            
            return response()->json([
                'status_code'   => 400,
                'message' => $e->getMessageBag()
            ], 400);
        }
	}

	public function authenticate(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
			}
			
			$user = JWTAuth::authenticate($token);

			if ($user->is_verified) {
				$this->transformer->setMode('complete');
				$user->token = JWTAuth::getToken()->get();
				return $this->response->item($user, $this->transformer);
			} else {
				return $this->response->errorUnauthorized('not_verified');
			}

        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return response()->json(compact('token'));
    }

	public function getUserByToken() 
	{
		try {
			
			if (! $user = JWTAuth::parseToken()->authenticate()) {
				return response()->json(['user_not_found'], 404);
			}

		} catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

			return response()->json(['token_expired'], $e->getStatusCode());

		} catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

			return response()->json(['token_invalid'], $e->getStatusCode());

		} catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
			
			return response()->json(['token_absent'], $e->getStatusCode());

		}

		if ($user->is_verified) {
			$this->transformer->setMode('basic');
			return $this->response->item($user, $this->transformer);
		}
		
		return $this->response->errorUnauthorized('not_verified');
	}

	public function updateDetails(Request $request, $id)
	{
		$result = $this->repository->updateDetails($id, $request);

		return $this->response->item($result, $this->transformer);
	}

	public function updatePassword(Request $request, $id)
	{
		try {
			$result = $this->repository->updatePassword($id, $request);
			
		} catch(\Dingo\Api\Exception\UpdateResourceFailedException $e) {

			throw $e;
		}
		
		return $this->response->noContent();
	}

	public function updateEmail(Request $request, $id)
	{
		$result = $this->repository->updateEmail($id, $request);
		return $this->response->item($result, $this->transformer);
	}


	public function verifyAccount($email, $token)
	{
		$user = $this->repository->verifyAccount($email, $token);

		if (!$user) {
			abort(400, 'invalid_token');
		}

		event(new UserVerifiedEmailEvent($user));

		return $this->response->noContent();
	}

	public function checkEmailExists(Request $request)
	{
		$email = $request->get('email', null);
		
		if ($email) {
			$exists = $this->repository->checkEmailExists($email);
			
			return $this->response->array(['exists' => $exists]);
		}

		abort(401, 'Missing email');
	}
}