<?php

namespace Sunnydevbox\TWUser\Http\Controllers\Admin;


class UserController extends \SleepingOwl\Admin\Http\Controllers\AdminController
{
	public function index()
	{
		return AdminDisplay::table()
	    ->setColumns([
	        AdminColumn::link('title')->setLabel('Title'),
	        AdminColumn::datetime('date')->setLabel('Date')->setFormat('d.m.Y')->setWidth('150px')
	    ]);
	}
}