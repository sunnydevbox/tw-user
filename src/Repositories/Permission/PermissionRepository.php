<?php

namespace Sunnydevbox\TWUser\Repositories\Permission;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;

class PermissionRepository extends BaseRepository implements CacheableInterface
{
	 use CacheableRepository;

    // Setting the lifetime of the cache to a repository specifically
     //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];



    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWUser\Validators\PermissionValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return config('tw-user.admin.permissions.model');
    }
}