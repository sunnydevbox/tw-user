<?php

namespace Sunnydevbox\TWUser\Repositories\Role;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;

class RoleRepository extends BaseRepository implements CacheableInterface
{
	 use CacheableRepository;

    // Setting the lifetime of the cache to a repository specifically
     //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];


    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWUser\Validators\RoleValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return config('tw-user.admin.roles.model');
    }
}