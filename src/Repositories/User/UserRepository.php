<?php

namespace Sunnydevbox\TWUser\Repositories\User;

//use Sunnydevbox\TWCore\Repositories\TWMetaTrait;
use Sunnydevbox\TWCore\Repositories\TWBaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserRepository extends TWBaseRepository
{
    //use TWMetaTrait;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return config('tw-user.api.users.model');
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWUser\Validators\UserValidator";
    }

    public function syncRoles($user_id, $roles)
    {
        $User = $this->find($user_id);
        
        if ($User) {
        
            $collection = collect($roles)->map(function ($item, $key) {
                return $key;
            });

            $User->syncRoles($collection->all());
        }

    }

    public function register($request)
    {
        $requestCollection = collect($request->all());

        $model = $this->makeModel(); 
        
        if (method_exists($model, 'metaFields')) {
            foreach ($model->metaFields() as $field) {
                $request->offsetUnset($field);
            }
        }
        
        //$this->makeValidator()->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
        if ($this->checkDuplicateEmail($request->get('email'))) {
            
            throw new \Exception('Duplicate email', 400);
        } else {

            $creds = [
                'email' => $request->get('email'),
                'password' => $request->get('password'),
                'verification_token' => $this->setVerificationToken(),
            ];
            // event(new Registered($user = $this->create($request->all())));
    
            // $this->guard()->login($user);
    
            // return $this->registered($request, $user)
            //                 ?: redirect($this->redirectPath());


            $result =  $this->create($creds);
            $result->saveMeta($requestCollection->only($model->metaFields()));
            
            // SEND VERIFICATION EMAIL
            // UPDATE THIS TO USE QUEUES
            // SENDING EMAIL IS LOCATED IN SendActivationCodeEventLister
            // Mail::to($request->get('email'))->send(new UserRegistered($result));
            
            return $result;
        }
    }

    public function setVerificationToken()
    {
        return str_random(25);
    }

    public function checkDuplicateEmail($email)
    {
        return  $this->makeModel()->where('email', $email)->first()? true : false;
    }


    public function updateDetails($id, $request)
    {
        if (!is_array($request)) {
            $request = $request->all();
        }

        $requestCollection = collect($request);
        
        $model = $this->makeModel(); 
        $result = $model->find($id);
        
        $result->saveMeta($requestCollection->only($model->metaFields()));

        return $result;
    }

    public function updateEmail($id, $request)
    {
        $result = $this->update(['email' => $request->get('new_email')] ,$id);

        return $result;
    }

    public function updatePassword($id, $request)
    {   
        // VALIDATION
        $validator = Validator::make($request->all(), [
            'password'          => 'required',
            'new_password'      => 'required|min:8|confirmed',
            'new_password_confirmation'   => 'required|min:8',
        ]);
        

        if ($validator->fails()) {
            throw new \Dingo\Api\Exception\UpdateResourceFailedException('Failed to update password', $validator->errors());
        } else {
            // AUTHENTICATE

            $user = JWTAuth::parseToken()->authenticate();
            if ($user->id != $id ) {
                throw  new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException('not_authorized');
            }


            if (!Hash::check($request->get('password'), $user->password)) {
                throw  new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException('invalid_password');
            }


            // UPDATE PASSWORD
            $result = $this->update(['password' => $request->get('new_password')] ,$id); 
            return true;
        }
    }

    public function checkEmailExists($email)
    {
        $status = $this->findByField('email', $email)->first();
        
        return ($status)  ? true : false;
    }


    public function verifyAccount($email, $token)
    {
        $email = trim(urldecode($email));

        $user = $this->model->where('email', '=', $email)
            ->where('verification_token', $token)
            ->first();

        return $user;
    }
}