<?php

namespace Sunnydevbox\TWUser;

use Illuminate\Support\ServiceProvider;

class TWUserServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application events.
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/api.php');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'tw-user'); 
    }

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->registerPublisher();
        $this->registerProviders();
        $this->registerCommands();
    }

    protected function registerProviders()
    {
        // EVENTS
        if (class_exists('\Sunnydevbox\TWUser\EventServiceProvider')
            && !$this->app->resolved('\Sunnydevbox\TWUser\EventServiceProvider')
        ) {
            $this->app->register(\Sunnydevbox\TWUser\EventServiceProvider::class);    
        }

        if (class_exists('\Sunnydevbox\TWCore\TWCoreServiceProvider')
            && !$this->app->resolved('\Sunnydevbox\TWCore\TWCoreServiceProvider')
        ) {
            $this->app->register(\Sunnydevbox\TWCore\TWCoreServiceProvider::class);    
        }


        // if (((class_exists('SleepingOwl\Admin\Providers\AdminSectionsServiceProvider')
        //         && !$this->app->resolved('SleepingOwl\Admin\Providers\AdminSectionsServiceProvider')))
        //     && (class_exists('Sunnydevbox\TWUser\Admin\Providers\AdminServiceProvider')
        //         && !$this->app->resolved('Sunnydevbox\TWUser\Admin\Providers\AdminServiceProvider'))
        // ) {
        //     $this->app->register(\Sunnydevbox\TWUser\Admin\Providers\AdminServiceProvider::class);    
        // }

        if (class_exists('\Spatie\Permission\PermissionServiceProvider')
            && !$this->app->resolved('\Spatie\Permission\PermissionServiceProvider')
        ) {
            $this->app->register(\Spatie\Permission\PermissionServiceProvider::class);    
        }
    }


    protected function registerCommands()
    {
        if ($this->app->runningInConsole()) {
            
            $this->commands([
                \Sunnydevbox\TWUser\Console\Commands\PublishMigrationsCommand::class,
                \Sunnydevbox\TWUser\Console\Commands\PublishConfigCommand::class,
                \Sunnydevbox\TWUser\Console\Commands\SetupCommand::class,
            ]);

            /*$localViewFactory = $this->createLocalViewFactory();
            $this->app->singleton(
                'command.sleepingowl.ide.generate',
                function ($app) use ($localViewFactory) {
                    return new \SleepingOwl\Admin\Console\Commands\GeneratorCommand($app['config'], $app['files'], $localViewFactory);
                }
            ); 

            $this->commands('command.sleepingowl.ide.generate');*/
        }
    }


    protected function registerPublisher()
    {
         $this->publishes([
            __DIR__ . '/../config/tw-user.php' => config_path('tw-user.php'),
        ], 'config');


        $this->publishes([
                __DIR__ . '/Admin/resources/default' => public_path('packages/tw-user'),
            ], 'assets');


        $this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/tw-user/'),
        ]);

        $this->mergeConfigFrom(
            __DIR__ . '/../config/tw-user.php', 'tw-user'
        );

        $timestamp = '2013_08_18_022643';//date('Y_m_d_His', time());
        $this->publishes([
            __DIR__.'/../migrations/create_users_table.php.stub' => database_path("/migrations/{$timestamp}_create_users_table.php"),
            __DIR__.'/../migrations/create_password_resets_table.php.stub' => database_path("/migrations/{$timestamp}_create_password_resets_table.php"),
        ], 'migrations');


        
    }
}