<?php
namespace Sunnydevbox\TWUser\Traits;

use Dingo\Api\Http\Request;

trait HasRolePermissionControllerTrait
{
    public function removeRole(Request $request, $id)
	{
		if (!empty($request->get('roles'))) {
			$user = $this->rpoUser->find($id);
			if ($user) {
				$user->removeRole($request->get('roles'));
			}

			return $this->response->noContent();
		}

		return $this->response->errorBadRequest('Missing roles');
	}

	public function assignRoles(Request $request, $id)
	{
		if (!empty($request->get('roles'))) {
			$user = $this->rpoUser->find($id);
			
			if ($user) {
				if (is_array($request->get('roles'))) {

					$user->syncRoles($request->get('roles'));
					
				} else if (is_string($request->get('roles'))) {

					$user->assignRole($request->get('roles'));

				}

				return response()->json([
					'status_code'   => 200,
					'message' => 'Roles assigned'
				], 200);
			}
		}

		return $this->response->errorBadRequest('Missing roles');
	}

	public function getRoles(RoleTransformer $roleTransformer, $id)
	{
		$user = $this->rpoUser->find($id);
		
		if ($user) {
			return $this->response->collection($user->roles()->get(), $roleTransformer);
			
			// return response()->json([
            //     'status_code'   => 200,
            //     'roles' => $user->getRoleNames()
            // ], 200);
		}
	}

	public function assignPermissions(Request $request, $id)
	{
		if (!empty($request->get('permissions'))) {
			$user = $this->rpoUser->find($id);
			
			if ($user) {
				if (is_array($request->get('permissions'))) {

					$user->syncPermissions($request->get('permissions'));

				} else if (is_string($request->get('permissions'))) {

					$user->givePermissionTo($request->get('permissions'));

				}

				return response()->json([
					'status_code'   => 200,
					'message' => 'Permissions granted'
				], 200);
			}
		}

		return $this->response->errorBadRequest('Missing permissions');
	}

	public function getPermissions(PermissionTransformer $permissionTransformer, $id)
	{
		$user = $this->rpoUser->find($id);
		
		if ($user) {
			
			return $this->response->collection($user->getAllPermissions(), $permissionTransformer);

			// return response()->json([
            //     'status_code'   => 200,
            //     'roles' => $user->getAllPermissions()
            // ], 200);
		}
	}
}