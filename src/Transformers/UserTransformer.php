<?php

namespace Sunnydevbox\TWUser\Transformers;

use Dingo\Api\Http\Request;
use Dingo\Api\Transformer\Binding;
use Dingo\Api\Contract\Transformer\Adapter;
use League\Fractal\TransformerAbstract;
//use Sunnydevbox\TWUser\Models\User;

class UserTransformer extends TransformerAbstract
{
     /**
     * Include user profile data by default
     */
    protected $defaultIncludes =   [ ];
    protected $availableIncludes = [
        'roles', 
        'permissions'
    ];

    protected $mode = 'complete'; // 'basic', 'complete'

    public function setMode(String $mode)
    {
        $this->mode = $mode;
    }

    public function transform($obj)
    {
        if ($this->mode == 'basic') {
            $data = [
                'id' => $obj->id,
            ];
        } else if ($this->mode == 'complete'){

            $data = [
                'id'            => (int) $obj->id,
                'email'         => $obj->email,

                'date_joined'       => date('Y-m-d H:i:s', strtotime($obj->created_at)),
                'last_update'   => date('Y-m-d H:i:s', strtotime($obj->updated_at)),

                'status'        => $obj->status,
                'is_verified'   => ($obj->is_verified) ? strtotime($obj->is_verified) : false,
            ];
            
            if (method_exists($obj,'getAllMeta')) {
                
                $metas = collect($obj->getAllMeta());
                
                foreach ($obj->metaFields() as $column_name) {
                    $data[$column_name] = $metas->get($column_name);
                }
            }
        }

        if (isset($obj->token)) {
            $data['token'] = $obj->token;
        }

        $this->setDefaultIncludes(['roles', 'permissions']);

        return $data;
    }

    public function includeRoles($model)
    {
        if ($model->roles) {
            return $this->collection($model->roles, new RoleTransformer());
        } else {
            return null;
        }
    }

    public function includePermissions($model) 
    {
        return $this->collection($model->getAllPermissions(), new PermissionTransformer());
    }
}