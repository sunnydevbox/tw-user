<?php

namespace Sunnydevbox\TWUser\Transformers;

use Dingo\Api\Http\Request;
use Dingo\Api\Transformer\Binding;
use Dingo\Api\Contract\Transformer\Adapter;
use League\Fractal\TransformerAbstract;

class PermissionTransformer extends TransformerAbstract
{

    public function transform($obj)
    {
        return [
            'id'        	=> (int) $obj->id,
            'name' 	    	=> $obj->name,
        ];
    }
}