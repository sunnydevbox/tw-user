<?php

namespace Sunnydevbox\TWUser\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Hash;
use Spatie\Activitylog\Traits\CausesActivity;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use HasRoles;
    use CausesActivity;

    protected $table = 'users';
    
    protected $guard_name = 'web';
    
    protected static $logAttributes = [
        'first_name', 
        'last_name', 
        'email',
        'verification_token',
        'remember_token',
        'is_verified',
        'status',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 
        'password',
        'verification_token',
        'remember_token',
        'is_verified',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',  // Comment this because BaseRepository forcFill()->array() is not able to pull this column
        //'remember_token',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = strtolower(trim($value));
    }

    public function setPasswordAttribute($value)
    {
        if(Hash::needsRehash($value) ) {
            $value = Hash::make($value);
        }
        $this->attributes['password'] = $value;
    }
}
