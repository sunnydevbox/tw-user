<?php

namespace Sunnydevbox\TWUser\Models;

use Spatie\Permission\Models\Role as SpatieRole;

use Spatie\Permission\Contracts\Role as RoleContract;

class Role extends SpatieRole implements RoleContract
{
	protected static $logAttributes = ['name', 'guard_name'];

	protected $hidden = [
		'updated_at',
		'created_at',
	];

	protected $fillable = [
		'name', 'guard_name',
	];
}