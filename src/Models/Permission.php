<?php

namespace Sunnydevbox\TWUser\Models;

use Spatie\Permission\Models\Permission as SpatiePermission;

class Permission extends SpatiePermission
{
	protected static $logAttributes = ['name', 'guard_name'];

	protected $hidden = [
		'updated_at',
		'created_at',
	];
	
	protected $fillable = [
		'name',
		'guard_name'
	];
	
	public function getProperNameAttribute()
	{
		return ucwords(str_replace('-', ' ', $this->attributes['name']));
	}

	public function setNameAttribute($value)
	{
		$this->attributes['name'] = str_replace(' ','-', trim(strtolower($value)));
	}

	// public function setGuardNameAttribute($value)
	// {
	// 	$this->attributes['guard_name'] = $value;
	// }
}