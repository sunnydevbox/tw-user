<?php

namespace Sunnydevbox\TWUser\Models;
use Phoenix\EloquentMeta\MetaTrait;

trait TWUserMetaTrait
{
    use MetaTrait;
    
    public function metaFields()
    {
        return $this->meta;
    }

    public function saveMeta($metas)
    {
        foreach($metas as $key => $value) {
            $this->updateMeta($key, $value);
        }

        return $this;
    }

    public function setMeta($key, $value) 
    {
        $this->saveMeta([$key => $value]);
    }
}