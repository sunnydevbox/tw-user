<p>Hi {{ $name }},</p>

<p>You have verified your email. Thank you</p>

<p>Welcome to RecoveryHub</p>

<h2>WHAT'S RECOVERYHUB?</h2>
 
<p>RecoveryHub is a space where teams of any size connect to get work done. Bring everyone together with group discussion, voice and video calling, document collaboration and more.
Having trouble getting started? Try pasting this link into your browser.</p>

<p>Cheers,<br/>
<strong>Recoveryhub Team</strong>
</p>