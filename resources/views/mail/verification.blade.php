<p>Hi {{ $user->getMeta('first_name') }},</p>

<p>You're almost ready to get started with Workplace. Please confirm your email, so we know it's really you.</p>
<p>Click on this link, <a href="{{ $verification_url }}">{{ $verification_url }}</a> , or copy and paste it into your browser's address bar to complete the process</p>



<h2>WHAT'S RECOVERYHUB?</h2>
 
<p>RecoveryHub is a space where teams of any size connect to get work done. Bring everyone together with group discussion, voice and video calling, document collaboration and more.
Having trouble getting started? Try pasting this link into your browser.</p>