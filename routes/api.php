<?php


if (app()->getProvider('\Dingo\Api\Provider\LaravelServiceProvider')) 
{
	$configUser = config('tw-user.api.users');
	$api = app('Dingo\Api\Routing\Router');

	$api->version('v1', ['middleware' => []], function ($api) use ($configUser) {
		
		$api->post('users/auth', [
			'as' => 'user.auth',
			'uses' => $configUser['controller'].'@authenticate',
		]);


		// $api->get('auth/token', [
		// 	'as' => 'auth.token',
		// 	'uses' => '\Sunnydevbox\TWCore\Http\Controllers\AuthenticateController@getAuthenticatedUser'

		// ]);
	   
	});

	$api->version('v1', ['middleware' => ['api.auth']], function ($api) use ($configUser) {

		$api->post('users/by-token', $configUser['controller'].'@getUserByToken')->name('user.by.token');


		$api->resource('users', 			$configUser['controller']);
		$api->put('users/details/{id}', 	$configUser['controller'].'@updateDetails')->name('users.update.details');
		$api->put('users/password/{id}', $configUser['controller'].'@updatePassword')->name('users.update.password');
		$api->put('users/email/{id}', 	$configUser['controller'].'@updateEmail')->name('users.update.email');

		$api->resource('roles', 		'\Sunnydevbox\TWUser\Http\Controllers\API\V1\RoleController');
		$api->resource('permissions', 	'\Sunnydevbox\TWUser\Http\Controllers\API\V1\PermissionController');
		$api->post('roles/{roleId}/attach-permission',	'\Sunnydevbox\TWUser\Http\Controllers\API\V1\RoleController@attachPermission');
		$api->post('roles/{roleId}/revoke-permission',	'\Sunnydevbox\TWUser\Http\Controllers\API\V1\RoleController@revokePermission');
		$api->get('roles/{roleId}/get-permissions',	'\Sunnydevbox\TWUser\Http\Controllers\API\V1\RoleController@getPermissions');

		$api->post('users/{id}/assign-roles', $configUser['controller'].'@assignRoles')->name('users.assignroles');
		$api->post('users/{id}/remove-role', $configUser['controller'].'@removeRole')->name('users.removerole');
		$api->get('users/{id}/roles', $configUser['controller'].'@getRoles')->name('users.getroles');

		$api->post('users/{id}/revoke-permissions', $configUser['controller'].'@assignPermissions')->name('users.revokepermissions');
		$api->post('users/{id}/assign-permissions', $configUser['controller'].'@assignPermissions')->name('users.assignpermissions');
		$api->get('users/{id}/permissions', $configUser['controller'].'@getPermissions')->name('users.getpermissions');

	});

	$api->version('v1', ['middleware' => []], function ($api) use ($configUser) {
		$api->post('users/register', $configUser['controller'].'@register')->name('users.register');
		$api->get('users/verify/{email}/{token}', $configUser['controller'].'@verifyAccount')->name('users.verify-account');
		$api->post('user/forgot-password', '\Sunnydevbox\TWUser\Http\Controllers\API\V1\ForgotPasswordController@sendResetLinkEmail')->name('users.forgot-password');

		$api->post('user/reset-password', '\Sunnydevbox\TWUser\Http\Controllers\API\V1\ResetPasswordController@reset')->name('user.reset-password');

		$api->post('user/email-check', '\Sunnydevbox\TWUser\Http\Controllers\API\V1\UserController@checkEmailExists')->name('user-check-email-exists');
		//$api->post('users/register', '\Sunnydevbox\TWUser\Http\Controllers\Auth\RegisterController@create')->name('users.register');
	});
}

//Route::post('users/register', '\Sunnydevbox\TWUser\Http\Controllers\Api\V1\UserController@register')->name('users.register');